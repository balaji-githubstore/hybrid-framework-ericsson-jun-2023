*** Settings ***
Documentation   This suite verify all valid users are allowed to 
...  access the OpenEMR Dashboard connected to TC_OH_02

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown       Close Browser

Test Template   Verify Valid Login Template

*** Test Cases ***
TC1
    admin   pass    English (Indian)    OpenEMR
TC2
    physician   physician   English (Indian)    OpenEMR
TC3
    [Template]  Verify Valid Login Template
    accountant      accountant  English (Indian)    OpenEMR

TC4
    [Documentation]     Example for overriding the test setup, test teardown, test template from settings
    [Setup]    
    [Template]
    Log To Console    bala
    [Teardown]      Log To Console    done

TC5
    [Documentation]     Example for overriding the test setup, test teardown,
    ...  using custom test template by overriding settings
    [Setup]
    [Template]  Print Value Template
    bala    ,welcome
    [Teardown]

*** Keywords ***
Verify Valid Login Template
    [Arguments]     ${username}     ${password}     ${language}     ${expected_title}
    Input Text    css=#authUser    ${username}
    Input Password    id=clearPass    ${password}
    Select From List By Label    name=languageChoice    ${language}
    Click Element    id=login-button
    Title Should Be    ${expected_title}

    
Print Value Template
    [Arguments]     ${data}     ${data2}
    Log To Console   HELLO ${data} ${data2}