*** Settings ***
Documentation   This suite verify all valid users are allowed to 
...  access the OpenEMR Dashboard connected to TC_OH_02

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown       Close Browser

Test Template   Verify Valid Login Template

*** Test Cases ***
TC1
    admin   pass    English (Indian)    OpenEMR
TC2
    physician   physician   English (Indian)    OpenEMR
TC3
    accountant      accountant  English (Indian)    OpenEMR

*** Keywords ***
Verify Valid Login Template
    [Arguments]     ${username}     ${password}     ${language}     ${expected_title}
    Input Text    css=#authUser    ${username}
    Input Password    id=clearPass    ${password}
    Select From List By Label    name=languageChoice    ${language}
    Click Element    id=login-button
    Title Should Be    ${expected_title}

    
