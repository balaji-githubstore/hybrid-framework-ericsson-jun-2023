*** Settings ***
Documentation   This suite verify all invalid users are not allowed to
...  access the OpenEMR Dashboard connected to TC_OH_03

Resource    ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch Browser And Navigate To Url
Test Teardown       Close Browser
Test Template   Verify Invalid Login Template

*** Test Cases ***
TC1 Verify Invalid Login Test
    saul   saul123    English (Indian)    Invalid username or password
TC2 Verify Invalid Login Test
    kim   kim123    English (Indian)    Invalid username or password

*** Keywords ***
Verify Invalid Login Template
    [Arguments]     ${username}     ${password}     ${language}     ${expected_error}
    Input Text    css=#authUser    ${username}
    Input Password    id=clearPass    ${password}
    Select From List By Label    name=languageChoice    ${language}
    Click Element    id=login-button
    Element Text Should Be    xpath=//p[contains(text(),'Invalid')]    ${expected_error}
