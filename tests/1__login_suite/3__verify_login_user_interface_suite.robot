*** Settings ***
Documentation   This suite verify UI components in the base page - TC_OH_01

Resource    ../../resource/base/CommonFunctionalities.resource

Suite Setup      Launch Browser And Navigate To Url
Suite Teardown       Close Browser

*** Test Cases ***
Verify Login Page Title
    Title Should Be    OpenEMR Login

Verify Application Description
    Element Text Should Be    xpath=//p[contains(text(),'most')]    The most popular open-source Electronic Health Record and Medical Practice Management solution.

Verify Username and Password Placeholder
    Element Attribute Value Should Be    id=authUser    placeholder    Username
    Element Attribute Value Should Be    id=clearPass    placeholder    Password
