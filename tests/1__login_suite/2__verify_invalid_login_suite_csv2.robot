*** Settings ***
Documentation   This suite verify all invalid users are not allowed to
...  access the OpenEMR Dashboard connected to TC_OH_03

Resource    ../../resource/base/CommonFunctionalities.resource

Library     DataDriver   file=../../test_data/verify_invalid_login_data2.csv     dialect=UserDefined    delimiter=.
Test Setup      Launch Browser And Navigate To Url
Test Teardown       Close Browser
Test Template   Verify Invalid Login Template

*** Test Cases ***
Verify Invalid Login Test_${test_case_name}

*** Keywords ***
Verify Invalid Login Template
    [Arguments]     ${username}     ${password}     ${language}     ${expected_error}
    Input Text    css=#authUser    ${username}
    Input Password    id=clearPass    ${password}
    Select From List By Label    name=languageChoice    ${language}
    Click Element    id=login-button
    Element Text Should Be    xpath=//p[contains(text(),'Invalid')]    ${expected_error}
